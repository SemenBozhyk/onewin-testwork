import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// loading components by async
export default new Router({
  mode: 'history',
  routes: [
    { path: '/fractions',
      component: () => import('v/fractions.vue'),
      name: "fractions"
    },
    { path: '/comments',
      component: () => import('v/comments.vue'),
      name: "comments"
    }
  ]
})
